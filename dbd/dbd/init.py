import msgpack
from dict_tools import data
from os import makedirs
from tempfile import gettempdir
from typing import Dict, List
from PyQt5.QtWidgets import (
    QApplication,
    QVBoxLayout,
    QWidget,
    QHBoxLayout,
    QProgressBar, QToolTip,
)
import pathlib


def __init__(hub):
    hub.dbd.SPLASH = QProgressBar()
    hub.dbd.SPLASH.setGeometry(0, 0, 300, 25)
    hub.dbd.ASSETS = pathlib.Path(str(gettempdir())).joinpath("dbd", "assets")


def cli(hub):
    hub.pop.config.load(["dbd", "idem", "acct"], cli="dbd")
    hub.dbd.CACHE = hub.dbd.ASSETS.joinpath(hub.OPT.dbd.build_cache)
    if hub.dbd.CACHE.exists():
        with open(hub.dbd.CACHE, "rb+") as fh_:
            contents = fh_.read()
            if contents:
                hub.dbd.BUILD = msgpack.loads(contents)
            else:
                hub.dbd.BUILD = []
    else:
        hub.dbd.BUILD = []
    makedirs(hub.dbd.ASSETS, exist_ok=True)
    hub.pop.sub.load_subdirs(hub.dbd)
    hub.pop.sub.add(dyne_name="idem")
    hub.pop.loop.create()
    hub.pop.Loop.run_until_complete(hub.dbd.init.app())


async def app(hub):
    hub.dbd.SPLASH.show()
    hub.dbd.APP.processEvents()

    # Load the data from the API
    perks = await hub.idem.ex.run("exec.dbd.api.perks", args=[], kwargs={})
    hub.dbd.PERKS = data.NamespaceDict(perks)
    hub.dbd.SPLASH.setMaximum(len(hub.dbd.PERKS))
    await hub.dbd.init.download_assets()
    hub.dbd.CATEGORIES = hub.dbd.init.categories()

    # Initialize layout items
    hub.dbd.BUILDS = hub.dbd.layout.builds.scroller()
    hub.dbd.SELECTED_PERK = {i: hub.dbd.layout.selections.icon() for i in range(4)}
    hub.dbd.ACTIVE_SELECTION = hub.dbd.SELECTED_PERK[0]
    hub.dbd.layout.perks.set_color(
        hub.dbd.ACTIVE_SELECTION.children()[1], hub.dbd.layout.perks.GREY
    )
    hub.dbd.SEARCH_BOX = hub.dbd.layout.search.box()
    hub.dbd.BUILD_NAME = hub.dbd.layout.name.text()
    hub.dbd.SELECTED_PERKS_GRID = hub.dbd.layout.selections.grid()
    hub.dbd.CATEGORY_GRID = hub.dbd.layout.category.grid()
    hub.dbd.ALL_PERKS = hub.dbd.layout.perks.grid()

    # Add builds from the cache
    await hub.dbd.init.load_builds()

    # Build the main app
    main_layout = hub.dbd.init.layout()
    hub.dbd.MAIN_WINDOW = MainWindow(main_layout)
    hub.dbd.SPLASH.hide()
    hub.dbd.MAIN_WINDOW.show()
    hub.dbd.MAIN_WINDOW.activateWindow()
    return hub.dbd.APP.exec_()


def layout(hub):
    widget = QWidget()
    right_side_vertical_layout = QVBoxLayout()
    right_side_vertical_layout.addWidget(hub.dbd.BUILD_NAME)
    right_side_vertical_layout.addWidget(hub.dbd.SELECTED_PERKS_GRID)
    right_side_vertical_layout.addWidget(hub.dbd.SEARCH_BOX)
    right_side_vertical_layout.addWidget(hub.dbd.CATEGORY_GRID)
    right_side_vertical_layout.addWidget(hub.dbd.ALL_PERKS)
    widget.setLayout(right_side_vertical_layout)

    main_layout = QHBoxLayout()
    main_layout.addWidget(hub.dbd.BUILDS)
    main_layout.addWidget(widget)

    return main_layout


class MainWindow(QWidget):
    def __init__(self, lay):
        super().__init__()
        self.setWindowTitle("DbD Companion")
        self.setLayout(lay)


def categories(hub) -> Dict[str, bool]:
    # TODO add the ability to filter by more attributes than just perkCategory
    ret = {}
    for perk in hub.dbd.PERKS.values():
        perk["categories"] = {c.split("::")[1] for c in perk["perkCategory"]}
        for category in perk["categories"]:
            ret[category] = False
    return ret


async def download_assets(hub):
    count = 0
    for name, perk in hub.dbd.PERKS.items():
        count += 1
        hub.dbd.SPLASH.setValue(count)
        icon_path = perk["iconPathList"][0]
        await hub.idem.ex.run(
            "exec.dbd.images.download", args=[], kwargs={"endpoint": icon_path}
        )


async def load_builds(hub):
    for build_data in hub.dbd.BUILD:
        build_name, perks = next(iter(build_data.items()))
        hub.dbd.layout.builds.add(build_name, perks)
