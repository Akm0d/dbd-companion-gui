from typing import Tuple
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QWidget, QLabel, QScrollArea, QVBoxLayout

__func_alias__ = {"id_": "id"}
BLACK = (0, 0, 0)
PURPLE = (64, 0, 64)
GREY = (120, 120, 120)
WHITE = (255, 255, 255)


def grid(hub):
    """
    A list of all the perks with their name and icon
    # TODO have sorting options besides alphabetical?
    """
    # TODO, Put this in a horizontal layout with left and right arrows
    # TODO don't, be so complicated, it's just another scroll area
    widget = QWidget()
    layout = hub.dbd.layout.init.flow()

    for perk in sorted(hub.dbd.PERKS):
        pixmap = hub.dbd.layout.perks.icon(perk)
        hub.dbd.PERKS[perk]["widget"] = pixmap
        icon_label_pair = hub.dbd.layout.perks.descriptive_icon(perk, pixmap)
        layout.addWidget(icon_label_pair)

    widget.setLayout(layout)
    scroll = QScrollArea()
    scroll.setWidget(widget)
    scroll.setWidgetResizable(True)
    return scroll


def description(hub, perk: str):
    description = hub.dbd.PERKS[perk]["perkDefaultDescription"]

    # Get the tunables from the api
    tunables = hub.dbd.PERKS[perk]["perkLevelTunables"]
    # Do a transform of the list of lists
    tunables = list(zip(*tunables))
    # Turn the tunables into sensible strings
    tunables = [f"({'|'.join(x)})" for x in tunables]
    # Apply the tunables to the descriptions
    return description.format(*tunables)


def icon(hub, perk: str):
    path = hub.dbd.layout.perks.icon_path(perk)
    pixmap = QPixmap(str(path))

    class ClickableQLabel(QLabel):
        def mousePressEvent(self, event) -> None:
            desc = hub.dbd.layout.perks.description(perk)
            display_name = hub.dbd.PERKS[perk]["displayName"]
            active_pixmap = hub.dbd.ACTIVE_SELECTION.children()[1]
            active_label = hub.dbd.ACTIVE_SELECTION.children()[2]

            hub.dbd.layout.perks.set_color(active_pixmap, PURPLE)
            active_pixmap.setPixmap(pixmap)
            active_pixmap.setToolTip(desc)
            active_pixmap.setStyleSheet("QToolTip {background-color: black; color: white; border: black solid 1px}")
            active_label.setText(display_name)

    label = ClickableQLabel()
    label.setPixmap(pixmap)
    label.setToolTip(hub.dbd.layout.perks.description(perk))
    hub.dbd.layout.perks.set_color(label, PURPLE)
    hub.dbd.layout.perks.set_defaults(label)
    label.setStyleSheet("QToolTip {background-color: black; color: white; border: black solid 1px}")
    label.perk = perk
    return label


def icon_path(hub, perk: str):
    return hub.dbd.ASSETS.joinpath(hub.dbd.PERKS[perk]["iconPathList"][0])


def descriptive_icon(hub, perk: str, pixmap: QLabel):
    """
    Pair a perk's image with it's description
    """
    display_name = hub.dbd.PERKS[perk]["displayName"]

    widget = QWidget()
    vertical_layout = QVBoxLayout()
    vertical_layout.addWidget(pixmap)
    vertical_layout.addWidget(QLabel(display_name))

    widget.setLayout(vertical_layout)
    return widget


def set_color(hub, pixmap: QLabel, color: Tuple[int, int, int]):
    pixmap.setStyleSheet(f"background-color: rgb{color}; color: #fff")


def set_defaults(hub, label: QLabel):
    label.perk = None
    label.setScaledContents(True)
    label.setAutoFillBackground(True)
    label.setFixedHeight(hub.OPT.dbd.icon_size)
    label.setFixedWidth(hub.OPT.dbd.icon_size)


def id_(hub, name: str) -> str:
    """
    Using a perk's name, retrieve it's name
    """
    for perk, data in hub.dbd.PERKS.items():
        if any(name == v for v in data.values()):
            return perk
    return ""
