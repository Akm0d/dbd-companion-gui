from PyQt5.QtWidgets import QLineEdit
import re

__func_alias__ = {"filter_": "filter"}


def box(hub):
    """
    Filter the "All Perks" grid by the text in this box based on each perk's name and description
    Only perks that lazily match the text will be shown.
    """
    box = QLineEdit()

    def _wrapper():
        hub.dbd.layout.search.filter()

    box.setPlaceholderText("\U0001F50D Search")
    box.returnPressed.connect(_wrapper)
    return box


def filter_(hub):
    """
    Filter perks based on the named category.
    The categories are defined by the API.
    Categories are allow-listed when checked
    """
    perk_widgets = hub.dbd.ALL_PERKS.children()[0].children()[0].children()[1:]
    search = hub.dbd.SEARCH_BOX.text().lower()
    search = "".join(c.lower() for c in search if c.isalpha())
    search = f".*{'.*'.join(search)}.*"
    pattern = re.compile(search)
    for widget in perk_widgets:
        widget.hide()
        perk = hub.dbd.PERKS[widget.children()[1].perk]
        perk_string = perk["displayName"]
        perk_string = "".join(c.lower() for c in perk_string if c.isalpha())

        if pattern.match(perk_string):
            widget.show()
