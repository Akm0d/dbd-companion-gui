from PyQt5.QtWidgets import QHBoxLayout, QWidget, QLabel, QVBoxLayout


def grid(hub):
    widget = QWidget()
    horizontal_layout = QHBoxLayout()
    for w in hub.dbd.SELECTED_PERK:
        horizontal_layout.addWidget(hub.dbd.SELECTED_PERK[w])
    horizontal_layout.addStretch()
    widget.setLayout(horizontal_layout)

    return widget


def icon(hub):
    class ClickableQLabel(QLabel):
        def mousePressEvent(self, event) -> None:
            active_pixmap = hub.dbd.ACTIVE_SELECTION.children()[1]
            if active_pixmap == self:
                active_label = hub.dbd.ACTIVE_SELECTION.children()[2]
                # If a perk was selected again then remove it
                hub.dbd.layout.perks.set_color(
                    active_pixmap, hub.dbd.layout.perks.BLACK
                )
                active_pixmap.setText(" ")
                active_label.setText(" ")
                hub.dbd.ACTIVE_SELECTION.setToolTip("Select a perk")
            else:
                hub.dbd.ACTIVE_SELECTION = self.parent()
                hub.dbd.layout.perks.set_color(self, hub.dbd.layout.perks.GREY)

    label = ClickableQLabel()

    label.setToolTip("Select a perk")
    label.setStyleSheet("QToolTip {background-color: black; color: white; border: black solid 1px}")
    hub.dbd.layout.perks.set_defaults(label)
    hub.dbd.layout.perks.set_color(label, hub.dbd.layout.perks.BLACK)

    return hub.dbd.layout.selections.descriptive_icon(label)


def descriptive_icon(hub, pixmap: QLabel):
    """
    Pair a perk's image with it's description
    """
    widget = QWidget()
    vertical_layout = QVBoxLayout()
    vertical_layout.addWidget(pixmap)
    vertical_layout.addWidget(QLabel(" "))

    widget.setLayout(vertical_layout)
    return widget
