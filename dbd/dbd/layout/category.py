from PyQt5.QtWidgets import QGridLayout, QWidget
from PyQt5.QtWidgets import QCheckBox

__func_alias__ = {"filter_": "filter"}


def grid(hub):
    """
    Filter the "All Perks" grid by the checked categories
    """
    widget = QWidget()
    layout = hub.dbd.layout.init.flow()

    categories = hub.dbd.CATEGORIES.copy()
    layout.addWidget(hub.dbd.layout.category.box("Slasher", True))
    layout.addWidget(hub.dbd.layout.category.box("Camper", True))
    # TODO clicking on "killer" or "survivor" will filter out categories that pertain to either killers or survivors

    for category in sorted(categories):
        layout.addWidget(hub.dbd.layout.category.box(category))

    widget.setLayout(layout)
    return widget


def box(hub, text: str, initial_state: bool = False) -> QCheckBox:
    """
    Categories are an allowlist of which perks can be shown.
    If the box is checked, then perks with that attribute will be put on the allow list
    """
    check = QCheckBox(text)
    check.setChecked(initial_state)
    hub.dbd.CATEGORIES[text] = initial_state

    def _wrapper(checked):
        hub.dbd.CATEGORIES[text] = checked
        hub.dbd.layout.category.filter()

    check.clicked.connect(_wrapper)
    return check


def filter_(hub):
    """
    Filter perks based on the named category.
    The categories are defined by the API.
    Categories are allow-listed when checked
    """
    perk_widgets = hub.dbd.ALL_PERKS.children()[0].children()[0].children()[1:]
    active_categories = {c for c in hub.dbd.CATEGORIES if hub.dbd.CATEGORIES[c]}
    active_categories = active_categories - {"Slasher", "Camper"}
    for widget in perk_widgets:
        name = widget.children()[1].perk
        perk = hub.dbd.PERKS[name]
        widget.hide()
        if "Camper" in perk["type"] and hub.dbd.CATEGORIES["Camper"]:
            widget.show()
        if "Slasher" in perk["type"] and hub.dbd.CATEGORIES["Slasher"]:
            widget.show()

        if active_categories:
            matches = perk["categories"] & active_categories
            if (
                (not hub.dbd.CATEGORIES["Camper"] and not hub.dbd.CATEGORIES["Slasher"])
                and matches
                and widget.isHidden()
            ):
                widget.show()
            elif not matches and not widget.isHidden():
                widget.hide()
