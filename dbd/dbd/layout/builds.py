import msgpack
from typing import List
from PyQt5.QtCore import QEvent, QObject
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import (
    QHBoxLayout,
    QVBoxLayout,
    QScrollArea,
    QWidget,
    QLabel,
    QPushButton,
)


def scroller(hub):
    """
    A list of builds
    # TODO load them from a cache on creation
    """
    widget = QWidget()
    vertical_layout = QVBoxLayout()
    widget.setLayout(vertical_layout)
    vertical_layout.addWidget(hub.dbd.layout.builds.new())
    # Put all the items on the top of the vertical layout
    vertical_layout.addStretch()
    scroll = QScrollArea()
    scroll.setWidget(widget)
    scroll.setWidgetResizable(True)
    scroll.setFixedWidth(300)
    return scroll


def item(hub, build_name: str = None, perks: List[str] = None):
    class HoverableQWidget(QWidget):
        def eventFilter(self, source: QObject, event: QEvent):
            if event.type() == QEvent.Enter:
                for c in range(2, 6):
                    self.children()[c].show()
            elif event.type() == QEvent.Leave:
                for c in range(2, 6):
                    self.children()[c].hide()

            return super(HoverableQWidget, self).eventFilter(source, event)

    widget = HoverableQWidget()
    widget.perks = perks or ["", "", "", ""]
    item = QHBoxLayout()
    item.addWidget(hub.dbd.layout.builds.label(build_name))
    item.addStretch()
    item.addWidget(hub.dbd.layout.builds.save())
    item.addWidget(hub.dbd.layout.builds.delete())
    item.addWidget(hub.dbd.layout.builds.export())
    item.addWidget(hub.dbd.layout.builds.show())
    widget.setLayout(item)
    widget.installEventFilter(widget)
    return widget


def label(hub, build_name: str = None):
    if build_name is None:
        build_name = "New Build"
    box = QLabel()
    box.setText(build_name)
    return box


def new(hub):
    def _wrapper(clicked):
        hub.dbd.layout.builds.add()
        hub.dbd.BUILD.append({"New Build": ["", "", "", ""]})
        hub.dbd.layout.builds.save_cache()

    button = hub.dbd.layout.builds.hover_button(
        "+", "Create a new empty build template slot", _wrapper
    )
    button.setMinimumWidth(200)
    button.show()

    return button


def save(hub):
    """
    Return a button that will save the active build to the slot this button belongs to
    """

    def _wrapper(clicked):
        # TODO update the cache at the right index
        new_label = hub.dbd.BUILD_NAME.text()
        new_perks = [
            hub.dbd.layout.perks.id(hub.dbd.SELECTED_PERK[i].children()[2].text())
            for i in range(4)
        ]
        button.parent().children()[1].setText(new_label)
        button.parent().perks = new_perks
        i = hub.dbd.layout.builds.index(button)
        hub.dbd.BUILD[i] = {new_label: new_perks}
        hub.dbd.layout.builds.save_cache()

    button = hub.dbd.layout.builds.hover_button(
        "<", "Save the active build template to this slot", _wrapper
    )

    return button


def delete(hub):
    """
    Delete the build in the slot that this button belongs to
    """

    def _wrapper(clicked):
        button.parent().deleteLater()
        i = hub.dbd.layout.builds.index(button)
        hub.dbd.BUILD.pop(i)
        hub.dbd.layout.builds.save_cache()

    button = hub.dbd.layout.builds.hover_button(
        "-", "Delete this build template and this slot", _wrapper
    )

    return button


def show(hub):
    """
    Show the build saved to the slot this button belongs to
    """

    def _wrapper(clicked):
        label = button.parent().children()[1].text()
        hub.dbd.BUILD_NAME.setText(label)
        hub.dbd.layout.builds.index(button)
        for i, perk in enumerate(button.parent().perks):
            active_pixmap = hub.dbd.SELECTED_PERK[i].children()[1]
            active_label = hub.dbd.SELECTED_PERK[i].children()[2]
            if not perk.strip():
                active_label = hub.dbd.SELECTED_PERK[i].children()[2]
                # If a perk was selected again then remove it
                hub.dbd.layout.perks.set_color(
                    active_pixmap, hub.dbd.layout.perks.BLACK
                )
                active_pixmap.setText(" ")
                active_label.setText(" ")
                hub.dbd.SELECTED_PERK[i].setToolTip("Select a perk")
            else:
                path = hub.dbd.layout.perks.icon_path(perk)
                desc = hub.dbd.layout.perks.description(perk)
                display_name = hub.dbd.PERKS[perk]["displayName"]
                pixmap = QPixmap(str(path))

                hub.dbd.layout.perks.set_color(
                    active_pixmap, hub.dbd.layout.perks.PURPLE
                )
                active_pixmap.setPixmap(pixmap)
                active_pixmap.setToolTip(desc)
                active_label.setText(display_name)

    button = hub.dbd.layout.builds.hover_button(
        ">",
        "Show the build template in this slot, making it the active build template",
        _wrapper,
    )

    return button


def export(hub):
    """
    Export the build saved to the slot this button belongs to as a json text file
    """

    def _wrapper(clicked):
        ...

    button = hub.dbd.layout.builds.hover_button(
        "↑", "Export the build template in this slot to a file", _wrapper
    )

    return button


def hover_button(hub, symbol: chr, tooltip: str, func):
    button = QPushButton(symbol)
    button.setStyleSheet("QToolTip {background-color: black; color: white; border: black solid 1px}")
    button.setToolTip(tooltip)
    button.setMaximumWidth(20)
    button.setMaximumHeight(20)
    button.clicked.connect(func)
    button.hide()
    return button


def add(hub, build_name: str = None, perks: List[str] = None):
    """
    Adds an empty build slot to the build list
    """
    # TODO save new builds to the existing cache
    empty_build_item = hub.dbd.layout.builds.item(build_name, perks)
    # The vertical layout has a + button and a spacer, add new empty builds before them
    layout = hub.dbd.BUILDS.widget().layout()
    second_to_last = len(layout) - 2
    layout.insertWidget(second_to_last, empty_build_item)


def index(hub, widget: QWidget) -> int:
    """
    Get the index of the widget in the vertical layout
    """
    horizontal_layout = widget.parent()
    vertical_layout: QVBoxLayout = horizontal_layout.parent()
    for i, child in enumerate(vertical_layout.children()[2:]):
        if widget.parent() == child:
            return i


def save_cache(hub):
    """
    Save the state of the builds to the cache
    """
    with open(hub.dbd.CACHE, "wb+") as fh_:
        fh_.write(msgpack.dumps(hub.dbd.BUILD))
