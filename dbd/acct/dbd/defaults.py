def __init__(hub):
    # Force acct to be unlocked to load defaults
    hub.acct.UNLOCKED = True


def gather(hub):
    if "default" not in hub.acct.SUB_PROFILES:
        return {
            "default": {
                "url": "https://dbd-stats.info",
                "branch": "live",
            }
        }
