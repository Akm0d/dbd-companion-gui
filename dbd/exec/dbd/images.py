import aiofiles
import aiohttp
import os


async def download(hub, ctx, endpoint):
    path = hub.dbd.ASSETS.joinpath(endpoint)
    if os.path.exists(path):
        return

    headers = {"pretty": "false", "branch": ctx.acct.branch}
    url = f"{ctx.acct.url}/data/Public/{endpoint}"

    async with aiohttp.ClientSession() as session:
        async with session.get(
            url,
            headers=headers,
        ) as response:
            os.makedirs(path.parent, exist_ok=True)
            async with aiofiles.open(path, "wb+") as fh:
                await fh.write(await response.read())
