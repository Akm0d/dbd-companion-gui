import aiohttp
import json

ENDPOINTS = [
    "maps",
    "perks",
    "offerings",
    "characters",
    "tunables",
    "emblemtunables",
    "customizationitems",
    "gameconfigs",
    "ranksthresholds",
    "itemaddons",
    "items",
    "storeoutfits",
    "config",
    "catalog",
    "news",
    "featured",
    "schedule",
    "bpevents",
    "specialevents",
    "archive",
    "archiverewarddata",
    "shrineofsecrets",
    # TODO "stats", # /api/state/:steam_64:
]


def _api_wrapper(hub, endpoint: str):
    async def _get_api(ctx, **headers):
        headers.update({"pretty": "false", "branch": ctx.acct.branch})
        url = f"{ctx.acct.url}/api/{endpoint}"
        if endpoint == "stats":
            url += ":steam_64"
        async with aiohttp.ClientSession() as session:
            async with session.get(
                url,
                headers=headers,
            ) as response:
                return json.loads(await response.text())

    return _get_api


def __func_alias__(hub):
    ret = {}
    for endpoint in ENDPOINTS:
        ret[endpoint] = _api_wrapper(hub, endpoint)
    return ret
