CLI_CONFIG = {}
CONFIG = {
    "icon_size": {"type": int, "default": 100},
    "build_cache": {"type": str, "default": "builds.json"},
}
DYNE = {
    "acct": ["acct"],
    "exec": ["exec"],
    "dbd": ["dbd"],
}
