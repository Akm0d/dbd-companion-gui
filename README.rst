=============
DbD Companion
=============

Installation for Development
============================

.. code-block:: bash

    git clone git@gitlab.com:Akm0d/dbd-companion-gui.git
    pip install -e dbd-companion-gui

CLI Usage
=========

.. code-block:: bash

    $ idem exec dbd.api.archive
    $ idem exec dbd.api.archiverewarddata
    $ idem exec dbd.api.bpevents
    $ idem exec dbd.api.catalog
    $ idem exec dbd.api.characters
    $ idem exec dbd.api.config
    $ idem exec dbd.api.customizationitems
    $ idem exec dbd.api.emblemtunables
    $ idem exec dbd.api.featured
    $ idem exec dbd.api.gameconfigs
    $ idem exec dbd.api.itemaddons
    $ idem exec dbd.api.items
    $ idem exec dbd.api.maps
    $ idem exec dbd.api.news
    $ idem exec dbd.api.offerings
    $ idem exec dbd.api.perks
    $ idem exec dbd.api.ranksthresholds
    $ idem exec dbd.api.schedule
    $ idem exec dbd.api.shrineofsecrets tome=Tome01
    $ idem exec dbd.api.specialevents
    $ idem exec dbd.api.storeoutfits
    $ idem exec dbd.api.tunables killer=Oni

App Usage
=========

.. code-block:: bash

    ./run.py
