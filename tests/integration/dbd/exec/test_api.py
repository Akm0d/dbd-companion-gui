import pytest


@pytest.mark.asyncio
async def test_api_call(hub, ctx, subtests):
    for endpoint in hub.exec.dbd.api.ENDPOINTS:
        with subtests.test(endpoint=endpoint):
            func = getattr(hub.exec.dbd.api, endpoint)
            # Make sure every function works and can be called
            result = await func(ctx)
