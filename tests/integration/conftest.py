import pytest


@pytest.fixture(scope="module")
def hub(hub):
    hub.pop.sub.add(dyne_name="idem")
    yield hub


@pytest.fixture(scope="module")
def acct_subs():
    return ["dbd"]


@pytest.fixture(scope="module")
def acct_profile() -> str:
    return "default"
